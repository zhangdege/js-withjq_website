/*这里就是本网站所有的函数的集结,仅包括jQuery*/
/*power by 张德豪 2019-12-28*/
/*变量名称用 ZDH_ 开头  例如：ZDH_reg*/
/*header style*/
$(document).ready(function(){
    $('#collection').click(function(){
        alert('收藏本站成功！');
    });
});
/*注册/注册页面*/
$(document).ready(function(){
    $("#zc").click(function () {
        var $ZDH_name = $('#rename').val();
        var $ZDH_password = $('#repassword').val();
        var $ZDH_repassword = $('#reconfirmPassword').val();
        var $ZDH_regName = /^[a-zA-Z][a-zA-Z0-9]{3,15}$/;  //姓名正则
        var $ZDH_regPwd = /^[a-zA-Z0-9]{4,10}$/;  //密码正则
        if (($ZDH_regName.test($ZDH_name) && $ZDH_regPwd.test($ZDH_password))&&($ZDH_password == $ZDH_repassword)){  //必须两者都成立才行，就是验证正则表达式和两个密码相同才行
            // alert('注册成功！');
            return true;
        }else {
            alert('用户名或者密码非法！');
            return false;
        }
    });
    $("#dl").click(function () {
        var $ZDH_name = $('#name').val();
        var $ZDH_password = $('#password').val();
        var $ZDH_regName = /^[a-zA-Z][a-zA-Z0-9]{3,15}$/; //姓名正则
        var $ZDH_regPwd = /^[a-zA-Z0-9]{4,10}$/;          //密码正则
        if (($ZDH_regName.test($ZDH_name)&& $ZDH_regPwd.test($ZDH_password)) || ($ZDH_name =='user1' && $ZDH_password == '123456')){  //正则表达式验证密码和提供的用户名
            return true;
        }else {
            alert('请输入正确的用户名和密码！');
            return false;
        }
    });
});
/*登陆注册模块结束标志*/
/*留言板的jQuery代码*/
$(document).ready(function(){
    $(".bbs header span").click(function(){
        $(".bbs .post").show();
    });
    var ZDH_tou=new Array("tou01.jpg","tou02.jpg","tou03.jpg","tou04.jpg");
    $(".post .btn").click(function(){
        var $ZDH_newLi=$("<li></li>");  //创建一个新的li节点元素
        var ZDH_iNum=Math.floor(Math.random()*4);  //随机获取头像
        var $ZDH_touImg=$("<div><img src='images/"+ZDH_tou[ZDH_iNum]+"'></div>");  //创建头像节点
        var $ZDH_title=$("<h1>"+$(".title").val()+"</h1>"); //设置标题节点
        var ZDH_newP=$("<p></p>");  //创建一个新的p节点元素
        var ZDH_myDate=new Date();
        var ZDH_currentDate=ZDH_myDate.getFullYear()+"-"+parseInt(ZDH_myDate.getMonth()+1)+"-"+ZDH_myDate.getDate()+" "+ZDH_myDate.getHours()+":"+ZDH_myDate.getMinutes();
        $(ZDH_newP).append("<span>版块："+$(".post select").val()+"</span>");  //在p节点中插入版块；
        $(ZDH_newP).append("<span>发表时间："+ZDH_currentDate+"</span>");     //在p节点中插入发布时间；
        $($ZDH_newLi).append($ZDH_touImg);  //插入头像
        $($ZDH_newLi).append($ZDH_title);   //插入标题
        $($ZDH_newLi).append(ZDH_newP);    //插入版块、时间内容
        $(".bbs section ul").prepend($ZDH_newLi);

        $(".post .content").val("");
        $(".post .title").val("");
        $(".post").hide();
    });
});
/*留言板模块结束标志*/
/*关于我们的代码*/
var $ZDH_dimeAxis=[{
    date:'2019.09',
    title:'初涉学jQuery',
    achievement:''
},
    {
        date:'2019.10',
        title:'前途迷茫',
        achievement:''
    },
    {
        date:'2019.10',
        title:'整一套《javaScript模块化综合教程》系列教材看完 ',
        achievement:'在线看视频实战（创新编程平台）'
    },
    {
        date:'2019.11',
        title:'《jQuery》系列教材也很轻易的看得懂，但是记忆力是硬伤',
        achievement:''
    },
    {
        date:'2019.11',
        title:'在线学习平台学习知识',
        achievement:'独自看得懂代码'
    },
    {
        date:'2019.11',
        title:'《jQuery课本》、《javaScript函数式编程》都看得差不多',
        achievement:''
    },
    {
        date:'2019.12',
        title:'博豪书店上线',
        achievement:''
    },
    {
        date:'2019.12',
        title:'博豪书店测试',
        achievement:''
    },
    {
        date:'2019.12',
        title:'一直专心做正版书籍的买卖',
        achievement:''
    },
    {
        date:'2019.12',
        title:'使用人数超过100个',
        achievement:''
    },
    {
        date:'2019.12',
        title:'要交作业了呀',
        achievement:''
    }];
$(function(){
    $.each($ZDH_dimeAxis,function(i,e){
        var ZDH_html='<li class="time-axis-item">'+
            '<div class="time-axis-date">'+e.date+'<span></span></div>'+
            '<div class="time-axis-title">'+e.title+'</div>'+
            '<p class="time-axis-achievement">'+e.achievement+'</p>'+
            '</li>';
        $('.time-axis').append(ZDH_html);
    });
});
/*关于我们的代码结束标志*/
/*抽奖环节代码开始标志*/
function randomnum(smin, smax) {// 获取2个值之间的随机数
    var ZDH_Range = smax - smin;
    var ZDH_Rand = Math.random();
    return (smin + Math.round(ZDH_Rand * ZDH_Range));
}
function runzp() {
    var ZDH_data = '[{"id":1,"prize":"590大洋","v":1.0},{"id":2,"prize":"100RMB","v":2.0},{"id":3,"prize":"安慰奖","v":48.0}]';// 奖项json
    var ZDH_obj = eval('(' + ZDH_data + ')');
    var ZDH_result = randomnum(1, 100);
    var ZDH_line = 0;
    var ZDH_temp = 0;
    var ZDH_returnobj = "0";
    var ZDH_index = 0;

    //alert("随机数"+result);
    for ( var ZDH_i = 0; ZDH_i < ZDH_obj.length; ZDH_i++) {
        var ZDH_obj2 = ZDH_obj[ZDH_i];
        var ZDH_c = parseFloat(ZDH_obj2.v);
        ZDH_temp = ZDH_temp + ZDH_c;
        ZDH_line = 100 - ZDH_temp;
        if (ZDH_c != 0) {
            if (ZDH_result > ZDH_line && ZDH_result <= (ZDH_line + ZDH_c)) {
                ZDH_index = ZDH_i;
                // alert(i+"中奖"+line+"<result"+"<="+(line + c));
                ZDH_returnobj = ZDH_obj2;
                break;
            }
        }
    }
    var ZDH_angle = 330;
    var ZDH_message = "";
    var ZDH_myreturn = new Object;
    if (ZDH_returnobj != "0") {// 有奖
        ZDH_message = "恭喜中奖了";
        var ZDH_angle0 = [ 344, 373 ];
        var ZDH_angle1 = [ 226, 256 ];
        var ZDH_angle2 = [ 109, 136 ];
        switch (ZDH_index) {
            case 0:// 一等奖
                var ZDH_r0 = randomnum(ZDH_angle0[0], ZDH_angle0[1]);
                ZDH_angle = ZDH_r0;
                break;
            case 1:// 二等奖
                var ZDH_r1 = randomnum(ZDH_angle1[0], ZDH_angle1[1]);
                ZDH_angle = ZDH_r1;
                break;
            case 2:// 三等奖
                var ZDH_r2 = randomnum(ZDH_angle2[0], ZDH_angle2[1]);
                ZDH_angle = ZDH_r2;
                break;
        }
        ZDH_myreturn.prize = ZDH_returnobj.prize;
    } else {// 没有
        ZDH_message = "再接再厉";
        var ZDH_angle3 = [ 17, 103 ];
        var ZDH_angle4 = [ 197, 220 ];
        var ZDH_angle5 = [ 259, 340 ];
        var ZDH_r = randomnum(3, 5);
        var ZDH_angle;
        switch (ZDH_r) {
            case 3:
                var ZDH_r3 = randomnum(ZDH_angle3[0], ZDH_angle3[1]);
                ZDH_angle = ZDH_r3;
                break;
            case 4:
                var ZDH_r4 = randomnum(ZDH_angle4[0], ZDH_angle4[1]);
                ZDH_angle = ZDH_r4;
                break;
            case 5:
                var ZDH_r5 = randomnum(ZDH_angle5[0],ZDH_angle5[1]);
                ZDH_angle = ZDH_r5;
                break;
        }
        ZDH_myreturn.prize = "继续努力!";

    }
    ZDH_myreturn.angle = ZDH_angle;
    ZDH_myreturn.message = ZDH_message;
    return ZDH_myreturn;
}
/*抽奖环节的代码结束标志*/
