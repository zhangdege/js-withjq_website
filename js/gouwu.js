﻿/*power by 张德豪 2019-12-28*/
/*变量名称用 ZDH_ 开头  例如：ZDH_reg*/
window.onload = function demo() {
	//找到全选框
	var ZDH_oCheckBoxAll = document.getElementById("checkAll");
	//找到全选框
	var ZDH_oCheck = document.getElementsByClassName("checkCss");
	//定义总共数量以及总金额
	var ZDH_totalCount = 0;
	var ZDH_totalMoney = 0;
	//找到显示数量和金额的元素
	var ZDH_priceTotal = document.getElementById("priceTotal");
	var ZDH_countTotal = document.getElementById("countTotal");
	var ZDH_P = document.getElementById('ZDH_price');  //输出到界面下
	var ZDH_T = document.getElementById("ZDH_total");
	//找到数量输入框的值
	var ZDH_oInputCount = document.getElementsByClassName("inputCountCss");
	//获取表格元素的值
	var ZDH_otab = document.getElementById("gwcTable");
	var ZDH_otr = document.getElementsByTagName("tr");
	//加减按钮元素
	var ZDH_oBtn_jian = document.getElementsByClassName("reduceCss");
	var ZDH_oBtn_jia = document.getElementsByClassName("addCss");
	//删除
	var ZDH_oDel = document.getElementsByClassName("a");
	//获取结算按钮id
	var ZDH_orderBtn = document.getElementById("btnOrder");
	//判断全选按钮是否勾选
	var ZDH_flag = 0;
	//单击全选按钮函数
	function Fcheck() {
		if(ZDH_oCheckBoxAll.checked == true) {
			for(var ZDH_i = 0; ZDH_i < ZDH_oCheck.length; ZDH_i++) {
				if(!ZDH_oCheck[ZDH_i].checked) { //判断单选框是否已经选中,不然价格和数量会重复加
					ZDH_oCheck[ZDH_i].checked = true;
					ZDH_flag+=1;
					ZDH_totalCount += parseInt(ZDH_oInputCount[ZDH_i].value);
					ZDH_totalMoney += parseInt(ZDH_otr[ZDH_i + 1].cells[4].innerText);
				}
			}
		} else {
			for(var ZDH_i = 0; ZDH_i < ZDH_oCheck.length; ZDH_i++) {
				ZDH_oCheck[ZDH_i].checked = false;
				ZDH_flag = 0;
			}
			ZDH_totalCount = 0;
			ZDH_totalMoney = 0;
		}
		Spantotal();
	}
	//显示总金额和总件数函数
	function Spantotal() {
		ZDH_countTotal.innerHTML = ZDH_totalCount;
		ZDH_priceTotal.innerHTML = ZDH_totalMoney;
		ZDH_T.innerHTML = ZDH_totalCount;   //生成订单
		ZDH_P.innerHTML = ZDH_totalCount;
	}
	//单选按钮函数
	function check() {
		for(var ZDH_i = 0; ZDH_i < ZDH_oCheck.length; ZDH_i++) {
			if(this == ZDH_oCheck[ZDH_i]) {
				var ZDH_index = ZDH_i;
				break;
			}
		}
		if(ZDH_oCheck[ZDH_index].checked) {
			ZDH_totalCount += parseInt(ZDH_oInputCount[ZDH_index].value);
			ZDH_totalMoney += parseInt(ZDH_oInputCount[ZDH_index].value) * parseInt(ZDH_otr[ZDH_index + 1].cells[2].innerText);
			ZDH_flag+= 1;
		} else {
			ZDH_totalCount -= parseInt(ZDH_oInputCount[ZDH_index].value);
			ZDH_totalMoney -= parseInt(ZDH_oInputCount[ZDH_index].value) * parseInt(ZDH_otr[ZDH_index + 1].cells[2].innerText);
			ZDH_flag-= 1;
		}
		if(ZDH_flag==ZDH_oCheck.length){
			ZDH_oCheckBoxAll.checked=true;
		}
		else{
			ZDH_oCheckBoxAll.checked=false;
		}
		Spantotal();
	}
	//删除函数
	function shanChu() {
		for(var ZDH_i = 0; ZDH_i < ZDH_oDel.length; ZDH_i++) {
			if(this == ZDH_oDel[ZDH_i]) {
				var ZDH_index = ZDH_i;
				break;
			}
		}
		if(ZDH_oCheck[ZDH_index].checked == true) { //点了删除按钮,则总金额和总数量减去该商品的数量和金额
			ZDH_totalCount -= parseInt(ZDH_oInputCount[ZDH_index].value);
			ZDH_totalMoney -= parseInt(ZDH_otr[ZDH_index + 1].cells[4].innerText);
		}
		Spantotal();
		demo();
		ZDH_otr[ZDH_index + 1].remove();
	}
	//减按钮功能函数
	function jian() {
		for(var ZDH_i = 0; ZDH_i < ZDH_oBtn_jian.length; ZDH_i++) {
			if(this == ZDH_oBtn_jian[ZDH_i]){
				var ZDH_index = ZDH_i;
				break;
			}
		}
		if(ZDH_oInputCount[ZDH_index].value != 0) {
			ZDH_oInputCount[ZDH_index].value = ZDH_oInputCount[ZDH_index].value - 1;
			ZDH_otr[ZDH_index+1].cells[4].innerText = parseInt(ZDH_oInputCount[ZDH_index].value) * parseInt(ZDH_otr[ZDH_index+1].cells[2].innerText);
			if(ZDH_oCheck[ZDH_index].checked && ZDH_oInputCount[ZDH_index].value != 0) {
				ZDH_totalCount -= 1;
				ZDH_totalMoney -= parseInt(ZDH_otr[ZDH_index+1].cells[2].innerText);
				Spantotal();
			}
		}
	}
	//加按钮功能函数
	function jia() {
		for(var ZDH_i = 0; ZDH_i < ZDH_oBtn_jian.length; ZDH_i++) {
			if(this == ZDH_oBtn_jia[ZDH_i]){
				var ZDH_index = ZDH_i;
				break;
			}
		}
		ZDH_oInputCount[ZDH_index].value = +(ZDH_oInputCount[ZDH_index].value) + 1;
		ZDH_otr[ZDH_index+1].cells[4].innerText = parseInt(ZDH_oInputCount[ZDH_index].value) * parseInt(ZDH_otr[ZDH_index+1].cells[2].innerText);
		if(ZDH_oCheck[ZDH_index].checked) {
			ZDH_totalCount += 1;
			ZDH_totalMoney += parseInt(ZDH_otr[ZDH_index+1].cells[2].innerText);
			Spantotal();
		}
	}
	function jiesuan(){
		if(ZDH_flag!=0){
			window.location.href="success.html";
		}
		else{
			alert("请至少选择一件商品结算");
		}
	}
	ZDH_oCheckBoxAll.onclick = Fcheck;			//调用全选函数
	for(var ZDH_i = 0; ZDH_i < ZDH_oCheck.length; ZDH_i++)
		ZDH_oCheck.item(ZDH_i).onclick = check; 	//调用单选函数
	for(var ZDH_i = 0; ZDH_i < ZDH_oDel.length; ZDH_i++)
		ZDH_oDel.item(ZDH_i).onclick = shanChu;			//调用删除函数
	for(var ZDH_i = 0; ZDH_i < ZDH_oBtn_jian.length; ZDH_i++)	//调用减
		ZDH_oBtn_jian.item(ZDH_i).onclick = jian;
	for(var ZDH_i = 0; ZDH_i < ZDH_oBtn_jia.length; ZDH_i++)	//调用加
		ZDH_oBtn_jia.item(ZDH_i).onclick = jia;
	ZDH_orderBtn.onclick=jiesuan;				//调用结算函数
}